# lab3_linux

![test](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image1.png)
# df
Возвращает информацию, сколько занимает файл на дисковой системе
# du
оценка использования файловой системы
# cp
Копирование файлов или директорий
# mv 
Перемещение (переименование) файлов
# ls 
Список содержимого папки(директории)
# ln
Создание ссылок между файлами
# ldd
Выводит зависимости общих объектов
# free
Отображение объёма свободной используемой памяти в системе
# dd
Конвертирование и копирование файла
# lsblk
Список блочных устройств
# blkid
Поиск/печать атрибутов блочного устройства
# parted
Программа работы с разделами
# fdisk
Программа работы с таблицой разделов диска
# ps
Отчёт-снапшот текущих процессов
# pstree
Отображение дерева процессов
# top
Отображение процессов Linux
# atop
Мониторинг просмотра нагрузки на систему Linux
# iotop
Отображает использование процессов с дисками
# htop
Отображает процессов
# vmstat
Отображает информацию об использовании памяти, дисков, процессора
# pidstat
Отображение статистических данных о процессе по его PID.
# iostat
вывод той же информации, что и top, только более скудно
---------------------------
Размер дисков разный

Неразмеченного места на дисках нет

Размер партиций различный

Таблица партицирования используется разная, т.к. используется две операционные системы

Последний вопрос не понял....
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image1.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image2.png)
# Создадим сжатую файловую систему для чтения squashhfs 
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image4.png)
# Просмотр информации по файловым системам, смонтированным в системе
dev/sda6
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image3.png)

Тип файловой системы ext4

Опции: монтирование файловой системы для чтения и записи, обнолвение времени доступа только при изменении файла или изменение времени доступа, при ошибке перемонтировать с параметром только для чтения, журналирование только метаданных без повреждения данных) 

128Кб размер файловой системы примонтированной в /mnt/mai

# Попробуем создать файлик в каталоге /dev/shm
tmpfs - временное файловое хранилище.

Изменялась общая память, буф./врем.
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image5.png)
# Изучим процессы запущенные в системе
systemd, postgres, lightdm, avahi-daemon, kthreadd, blueberry-tray

Мультитредовые

/sbin/init splash

[kthreadd]
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image6.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image7.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image8.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image9.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image10.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/image11.png)


# Разобраться что делает команда 
ps axo rss | tail -n +2|paste -sd+ | bc

Команда подсчитывает количество КБ процессов

# Уставновим утилиту smem и проанализируем параметр PSS в ней

![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/20.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/21.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/222.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/16.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/17.png)

# Запуск приложенного скрипта и наблюдения за процессом
Состояния у процессов следующие: S, Z

Процессы со статусом S - спят

Процессы со статусом Z - зомби

PID у основного процесса (bash), но и тот приводит к процессу Systemd, отцом которого является root.

PID бомбы был 7668

убив PPID, программа переключила PPID на 1309 на Systemd

![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/12.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/13.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/14.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/15.png)

Вроде разобрался хорошо, но стоит проверить лишний раз.

Он создаёт копии самого себя, оставляя зомби в списке процессов, которых необходимо ликвидировать. 

Если убить головной процесс, тогда он автоматом переключается на Systemd процесс, но когда закончил забивать процессами, зомби сами умирают, остаётся только один, и тот спит, пока не дойдёт до критической точки, после которой сообщит, что он и является отцовским процессом, который плодит зомби.

## Научимся убивать зомби процессы
Зомби процессы, задержанные утилитой

![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/17.png)
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/16.png)

# проблемы при отмонтировании директории
Попытки отмонтировать директорию приводят к ошибке: процесс занят
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/14.png)
## Решаем загадку исчезновения места на диске
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/18.png)
Результат удаления файла под root'ом привёл к очищению файлов
![blkrd](https://gitlab.com/wolkoduff/lab3_linux/blob/master/image/23232.png)